#!/usr/bin/env python
#
# Copyright (c) 2010-2015 Martin Natano <natano@natano.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import shlex
import subprocess
from collections import defaultdict
from setuptools import setup, Extension


# XXX: ugly hack for manpages
if 'install' in sys.argv:
    sys.argv += ['--single-version-externally-managed', '--record=/dev/null']


def pkg_config(*libs):
    flags = defaultdict(list)

    try:
        cflags = subprocess.check_output(['pkg-config', '--cflags'] + list(libs))
        libs = subprocess.check_output(['pkg-config', '--libs'] + list(libs))
    except subprocess.CalledProcessError as e:
        sys.exit(str(e))

    for flag in shlex.split(cflags):
        if flag.startswith('-I'):
            flags['include_dirs'].append(flag[2:])
        elif flag.startswith('-D'):
            name, eq, value = flag[2:].partition('=')
            flags['define_macros'].append((name, value if eq else None))
        elif flag.startswith('-U'):
            flags['undef_macros'].append(flag[2:])
        else:
            flags['extra_compile_args'].append(flag)

    for flag in shlex.split(libs):
        if flag.startswith('-L'):
            flags['library_dirs'].append(flag[2:])
        elif flag.startswith('-l'):
            flags['libraries'].append(flag[2:])
        else:
            flags['extra_link_args'].append(flag)

    return dict(flags)


pdf_module = Extension(
    'DouF00.pdf',
    ['pysrc/pdfmodule.cc'],
    **pkg_config('poppler', 'poppler-splash')
)


setup(
    name='DouF00',
    version='3.0.2',
    description='fat free presentations',
    author='Martin Natano',
    author_email='natano@natano.net',
    license='MIT',
    url='https://github.com/natano/presentation',
    platforms=['Linux'],
    packages=['DouF00'],
    package_dir={'': 'pysrc'},
    data_files=[
      ('man/man1', ['doc/douf00.1']),
    ],
    ext_modules=[pdf_module],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: X11 Applications',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Multimedia :: Graphics :: Presentation',

    ],
    entry_points={
        'console_scripts': [
            'douf00 = DouF00.douf00:main',
        ],
    },
    install_requires=['wxPython >= 2.8'],
)
