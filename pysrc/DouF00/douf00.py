# Copyright (c) 2010-2015 Martin Natano <natano@natano.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import sys
import time
import atexit

import wx

from DouF00 import appcfg
from DouF00.PresentorsScreen import PresentorsScreen
from DouF00.PresentationScreen import PresentationScreen
from DouF00.NumberFrame import NumberFrame
from DouF00.DisplayChoice import DisplayChoice
from DouF00.ImageList import ImageList
from DouF00.usercfg import config
from DouF00.utils import guessFiletype
from DouF00 import pdf


class MyApp(wx.App):
    def __init__(self):
        super(MyApp, self).__init__()
        self.timer_id = wx.NewId()
        self.timer = wx.Timer()

    def OnInit(self):
        self.presentationScreens = []
        self.presentorsScreens = []

        config.loadFile(appcfg.configFile)
        parser, args = config.loadArgv(sys.argv)

        if config['predouf00']:
            os.system(config['predouf00'])
        if config['postdouf00']:
            atexit.register(lambda: os.system(config['postdouf00']))

        if len(args) > 1:
            parser.error('Only one slidepath is supported.')
        elif len(args) == 1:
            slidepath = args[0]
        else:
            if config['slidepath']:
                slidepath = config['slidepath']
            else:
                slidepath = wx.FileSelector(
                    'Choose a file to open', wildcard='*.pdf')
                if not slidepath:
                    print >>sys.stderr, parser.format_help()
                    sys.exit('No path specified')

        slidetype = guessFiletype(slidepath)

        if not slidetype == 'PDF':
            if config['password']:
                parser.error('Option -S is only suitable for PDF files')
            if config['blankpage']:
                parser.error('Option -B is only supported with PDF files')

        pdfpass = None
        if config['password']:
            pdfpass = wx.GetPasswordFromUser('PDF password')

        if slidetype == 'dir':
            if config['blankslide']:
                config['blankslide'] = os.path.abspath(config['blankslide'])

            try:
                os.chdir(slidepath)
            except OSError:
                sys.exit('No such file or directory')

            appcfg.pictureFiles = []
            files = os.listdir(os.getcwd())

            # support for more picture types
            for fname in files:
                if guessFiletype(fname) in ('JPEG', 'PNG', 'BMP', 'PCX'):
                    appcfg.pictureFiles.append(fname)

            appcfg.pictureFiles.sort()

            if config['blankslide']:
                if guessFiletype(config['blankslide']) in ('JPEG', 'PNG',
                                                           'BMP', 'PCX'):
                    appcfg.blankslide = ('image', config['blankslide'])
                    if appcfg.blankslide[1] in appcfg.pictureFiles:
                        appcfg.pictureFiles.remove(appcfg.blankslide[1])
                else:
                    sys.exit('File type not supported')

            else:
                appcfg.blankslide = ''

        elif slidetype == 'PDF':
            pdf.load(os.path.abspath(slidepath), password=pdfpass)
            appcfg.pictureFiles = []
            for i in xrange(pdf.get_n_pages()):
                appcfg.pictureFiles.append(i)

            if config['blankslide']:
                if guessFiletype(config['blankslide']) in ('JPEG', 'PNG',
                                                           'BMP', 'PCX'):
                    appcfg.blankslide = ('image', config['blankslide'])
                    if appcfg.blankslide[1] in appcfg.pictureFiles:
                        appcfg.pictureFiles.remove(appcfg.blankslide[1])
                else:
                    sys.exit('File type not supported')

            elif not config['blankpage'] == 0:
                appcfg.blankslide = ('PDF', config['blankpage'] - 1)
                appcfg.pictureFiles.remove(appcfg.blankslide[1])

            else:
                appcfg.blankslide = ''

        elif slidetype is None:
            sys.exit('File type not supported')

        appcfg.slidelist = ImageList()

        displayCount = wx.Display.GetCount()
        self.numberFrames = []
        for d in xrange(displayCount):
            self.numberFrames.append(NumberFrame(d))

        self.choice = DisplayChoice()
        self.choice.button.Bind(wx.EVT_BUTTON, self.Run)
        if config['autostart']:
            self.Run(None)

        return True

    def OnKeyPress(self, event):
        event.Skip()
        key = event.GetKeyCode()
        if key in (wx.WXK_RIGHT, wx.WXK_SPACE, wx.WXK_PAGEDOWN):
            self.NextSlide(1)
        elif key in (wx.WXK_LEFT, wx.WXK_PAGEUP):
            self.NextSlide(-1)
        elif key == wx.WXK_RETURN:
            self.exitIndex()
        elif key == wx.WXK_DOWN:
            if appcfg.index:
                self.NextSlide(3)
        elif key == wx.WXK_UP:
            if appcfg.index:
                self.NextSlide(-3)
        elif key in (ord('q'), ord('Q')):
            sys.exit()
        elif key in (ord('r'), ord('R')):
            self.startTime = int(time.time())
            self.elapsedTime = 0
        elif key in (ord('p'), ord('P')):
            appcfg.pause = not appcfg.pause
        elif key in (ord('s'), ord('S')):
            self.swapScreens()
        elif key in (ord('i'), ord('I'), wx.WXK_ESCAPE, wx.WXK_F5):
            if appcfg.index:
                for p in self.presentationScreens:
                    p.load(self.slideindex)
            appcfg.index = not appcfg.index
            for p in self.presentorsScreens:
                p.index(self.slideindex)

    def swapScreens(self):
        presentationScreens = []
        presentorsScreens = []
        for i in xrange(len(self.presentationScreens)):
            displayindex = self.presentationScreens[i].displayindex
            self.presentationScreens[i].Destroy()
            p = PresentorsScreen(displayindex)
            p.load(self.slideindex)
            presentorsScreens.append(p)

        for i in xrange(len(self.presentorsScreens)):
            displayindex = self.presentorsScreens[i].displayindex
            self.presentorsScreens[i].index(self.slideindex, force=True)
            self.presentorsScreens[i].Destroy()
            p = PresentationScreen(displayindex)
            p.load(self.slideindex)
            presentationScreens.append(p)

        self.presentationScreens = presentationScreens
        self.presentorsScreens = presentorsScreens
        for p in self.presentationScreens:
            p.load(self.slideindex)
            p.panel.Bind(wx.EVT_KEY_UP, self.OnKeyPress)
            for thing in (p, p.panel, p.static_bitmap):
                thing.Bind(wx.EVT_LEFT_DOWN, self.OnLeftClick)
                thing.Bind(wx.EVT_RIGHT_DOWN, self.OnRightClick)

            p.panel.SetFocus()

        for p in self.presentorsScreens:
            p.load(self.slideindex)
            p.panel.Bind(wx.EVT_KEY_UP, self.OnKeyPress)
            for thing in (p, p.panel, p.clock, p.countUp, p.countDown,
                          p.static_bitmap[0], p.static_bitmap[1]):
                thing.Bind(wx.EVT_LEFT_DOWN, self.OnLeftClick)
                thing.Bind(wx.EVT_RIGHT_DOWN, self.OnRightClick)

            p.panel.SetFocus()

    def OnLeftClick(self, event):
        event.Skip()
        self.NextSlide(1)

    def OnRightClick(self, event):
        event.Skip()
        self.NextSlide(-1)

    def exitIndex(self):
        if appcfg.index:
            appcfg.index = not appcfg.index
            for p in self.presentorsScreens:
                p.index(self.slideindex)
            for p in self.presentationScreens:
                p.load(self.slideindex)

    def Run(self, event):
        self.slideindex = -1
        self.runTime = self.choice.spinctrl.GetValue() * 60
        self.startTime = int(time.time())
        self.elapsedTime = 0

        displayCount = wx.Display.GetCount()
        for displayindex in xrange(displayCount):
            selection = self.choice.choices[
                self.choice.selections[displayindex].GetSelection()]
            if selection == 'Audience':
                self.presentationScreens.append(
                    PresentationScreen(displayindex))
            elif selection == 'Presentor':
                self.presentorsScreens.append(
                    PresentorsScreen(displayindex))

        self.choice.Destroy()
        for numberFrame in self.numberFrames:
            numberFrame.Destroy()

        if not self.presentationScreens and not self.presentorsScreens:
            sys.exit()

        for p in self.presentationScreens:
            p.load(self.slideindex)
            p.panel.Bind(wx.EVT_KEY_UP, self.OnKeyPress)
            for thing in (p, p.panel, p.static_bitmap):
                thing.Bind(wx.EVT_LEFT_DOWN, self.OnLeftClick)
                thing.Bind(wx.EVT_RIGHT_DOWN, self.OnRightClick)

            p.panel.SetFocus()

        for p in self.presentorsScreens:
            p.load(self.slideindex)
            p.panel.Bind(wx.EVT_KEY_UP, self.OnKeyPress)
            for thing in (p, p.panel, p.clock, p.countUp, p.countDown,
                          p.static_bitmap[0], p.static_bitmap[1]):
                thing.Bind(wx.EVT_LEFT_DOWN, self.OnLeftClick)
                thing.Bind(wx.EVT_RIGHT_DOWN, self.OnRightClick)

            p.panel.SetFocus()

        self.setClock()
        self.timer.Start(100)
        self.Bind(wx.EVT_TIMER, self.setClock, self.timer)

    def setClock(self, event=None):
        now = int(time.time())

        if (self.slideindex < 0 or
            self.slideindex >= len(appcfg.pictureFiles) or
            self.elapsedTime >= self.runTime or appcfg.pause):
            self.startTime = now - self.elapsedTime

        self.elapsedTime = now - self.startTime
        remainingTime = self.runTime - self.elapsedTime
        for s in self.presentorsScreens:
            s.clock.SetLabel(time.strftime('%H:%M:%S'))
            countUpStr = '  {:02d}:{:02d}  '.format(
                self.elapsedTime / 60, self.elapsedTime % 60)
            countDownStr = '  {:02d}:{:02d}  '.format(
                remainingTime / 60, remainingTime % 60)
            s.countUp.SetLabel(countUpStr)
            s.countDown.SetLabel(countDownStr)
            if remainingTime < 120:
                color = wx.Colour(255, 0, 0)
                s.countDown.SetForegroundColour(color)

            try:
                red = 255 * self.elapsedTime / self.runTime
                green = 150 * remainingTime / self.runTime
            except ZeroDivisionError:
                red = 255
                green = 0
            color = wx.Colour(red, green, 0)
            s.panel.SetBackgroundColour(color)

    def NextSlide(self, step):
        if (self.slideindex + step > len(appcfg.pictureFiles)) and \
            config['exitafterlastslide'] and not appcfg.index:
            sys.exit()

        if (self.slideindex + step <= len(appcfg.pictureFiles) and
            self.slideindex + step >= -1):
            self.slideindex += step

        if not appcfg.index:
            for p in self.presentationScreens:
                p.load(self.slideindex)

        for p in self.presentorsScreens:
            p.load(self.slideindex, prevSlide=self.slideindex - step)


def main():
    app = MyApp()
    app.MainLoop()
