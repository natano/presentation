# Copyright (c) 2010-2015 Martin Natano <natano@natano.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import wx

from DouF00 import appcfg
from DouF00 import pdf


class ImageList(object):
    def __init__(self):
        super(ImageList, self).__init__()
        if not appcfg.blankslide:
            self.blank = None
        elif appcfg.blankslide[0] == 'image':
            self.blank = wx.Image(appcfg.blankslide[1])
        elif appcfg.blankslide[0] == 'PDF':
            self.blank = self.loadPdfPage(appcfg.blankslide[1])
        else:
            raise KeyError('Unknown blank slide format')

    def loadImage(self, slideindex, size):
        image = wx.Image(appcfg.pictureFiles[slideindex])
        image = image.scaleTo(size)
        return image

    def loadPdfPage(self, pagenum, size):
        width, height, buf = pdf.render_page(pagenum, size)
        return wx.ImageFromBuffer(width, height, buf)

    def _empty(self, size):
        buf = '\0\0\0' * size[0] * size[1]
        return wx.ImageFromBuffer(size[0], size[1], buf)

    def get(self, slideindex, size):
        slidecount = len(appcfg.pictureFiles)
        if slideindex < 0 or slideindex == slidecount:
            image = self.blank or self._empty(size)
        elif slideindex > slidecount:
            image = None
        elif pdf.get_filename():
            image = self.loadPdfPage(slideindex, size)
        else:
            image = self.loadImage(slideindex, size)
        return image
