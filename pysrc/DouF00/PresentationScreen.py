# Copyright (c) 2010-2015 Martin Natano <natano@natano.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import wx

from DouF00 import appcfg
from DouF00.FullscreenFrame import FullscreenFrame


class PresentationScreen(FullscreenFrame):
    def __init__(self, displayindex):
        style = wx.NO_BORDER | wx.STAY_ON_TOP
        super(PresentationScreen, self).__init__(
            displayindex, None, title=appcfg.title, style=style)
        box = wx.BoxSizer(wx.VERTICAL)
        self.SetBackgroundColour(wx.Colour(0, 0, 0))
        self.static_bitmap = wx.StaticBitmap(self)
        box.Add(self.static_bitmap, 0, wx.ALIGN_CENTER)
        self.SetSizer(box)
        self.panel = wx.Panel(self, size=(0, 0), pos=(0, 0))
        self.panel.SetBackgroundColour(wx.Colour(0, 0, 0))
        self.Show()

    def load(self, slideindex):
        image = appcfg.slidelist.get(slideindex, self.GetSize())
        bitmap = wx.BitmapFromImage(image)
        self.static_bitmap.SetBitmap(bitmap)
        self.Layout()
