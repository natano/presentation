/*
 * Copyright (c) 2013-2015 Martin Natano <natano@natano.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <Python.h>

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include <GlobalParams.h>
#include <PDFDoc.h>
#include <goo/GooString.h>
#include <splash/SplashBitmap.h>
#include <SplashOutputDev.h>


static PyObject	*pdf_load(PyObject *, PyObject *, PyObject *);
static PyObject	*pdf_unload(PyObject *, PyObject *);
static PyObject	*pdf_get_filename(PyObject *, PyObject *);
static PyObject	*pdf_get_n_pages(PyObject *, PyObject *);
static PyObject	*pdf_render_page(PyObject *, PyObject *);


static PDFDoc *doc = NULL;
static SplashOutputDev *splashOut;

static PyMethodDef PdfMethods[] = {
	{"load", (PyCFunction)pdf_load, METH_VARARGS | METH_KEYWORDS, "Load PDF file"},
	{"unload", pdf_unload, METH_NOARGS, "Unload PDF file"},
	{"get_filename", pdf_get_filename, METH_NOARGS, "return path of PDF file"},
	{"get_n_pages", pdf_get_n_pages, METH_NOARGS, "return number of pages"},
	{"render_page", pdf_render_page, METH_VARARGS, "Render Page"},
	{NULL, NULL, 0, NULL}
};


static PyObject *
pdf_load(PyObject *self, PyObject *args, PyObject *kw)
{
	char *keywords[] = {"filename", "password", NULL};
	char *c_filename, *c_password = NULL;
	GooString *filename, *password = NULL;

	if (!PyArg_ParseTupleAndKeywords(
	    args, kw, "s|z", keywords, &c_filename, &c_password))
		return (NULL);

	filename = new GooString(c_filename);
	if (c_password)
		password = new GooString(c_password);

	delete doc;
	doc = new PDFDoc(filename, password, password, NULL);

	delete password;

	if (!doc->isOk()) {
		delete doc;
		doc = NULL;

		PyErr_SetString(PyExc_IOError, "Failed to open file");
		return (NULL);
	}

	splashOut->startDoc(doc);
	Py_RETURN_NONE;
}

static PyObject *
pdf_unload(PyObject *self, PyObject *args)
{
	delete doc;
	doc = NULL;
	Py_RETURN_NONE;
}

static PyObject *
pdf_get_filename(PyObject *self, PyObject *args)
{
	if (!doc)
		Py_RETURN_NONE;

	return Py_BuildValue("s", doc->getFileName()->getCString());
}

static PyObject *
pdf_get_n_pages(PyObject *self, PyObject *args)
{
	if (!doc)
		Py_RETURN_NONE;

	return Py_BuildValue("i", doc->getNumPages());
}

static PyObject *
pdf_render_page(PyObject *self, PyObject *args)
{
	int pg, width = 0, height = 0;
	double pg_w, pg_h, resolution;
	SplashColorPtr data;
	SplashBitmap *bitmap;
	PyObject *ret;

	if (!doc)
		Py_RETURN_NONE;

	if (!PyArg_ParseTuple(args, "i(ii)", &pg, &width, &height))
		return (NULL);
	pg += 1;	// poppler starts page indices at 1

	pg_w = doc->getPageCropWidth(pg);
	pg_h = doc->getPageCropHeight(pg);
	if (doc->getPageRotate(pg) % 180 == 90)
		std::swap(pg_w, pg_h);

	resolution = 72.0 * std::min(width / pg_w, height / pg_h);

	doc->displayPage(splashOut, pg, resolution, resolution, 0, gFalse,
	    gTrue, gFalse);

	bitmap = splashOut->getBitmap();
	width = bitmap->getWidth();
	height = bitmap->getHeight();
	data = bitmap->getDataPtr();
	ret = Py_BuildValue("iis#", width, height, data, width * height * 3);
	return (ret);
}

PyMODINIT_FUNC
initpdf(void)
{
	PyObject *m;
	SplashColor white = {255, 255, 255};

	m = Py_InitModule("DouF00.pdf", PdfMethods);
	if (m == NULL)
		return;

	globalParams = new GlobalParams();

	splashOut = new SplashOutputDev(splashModeRGB8, 3, gFalse, white);
	splashOut->setVectorAntialias(gTrue);
	splashOut->setFreeTypeHinting(gTrue, gFalse);
}
